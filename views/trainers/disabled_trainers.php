<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\trainers\trainers;
$objLoginUser = new user_login();
$objLoginUser -> login_check();
$objallDisableTrainers = new trainers();
$allDisableTrainer = $objallDisableTrainers -> disable_trainers_list();

include_once '../header.php';
include_once 'menubar.php';
?>
<?php $objallDisableTrainers -> session_message('TrainerRestored'); ?>
<div class="panel panel-flat">
	<div class="panel-body">
		<h3 class="content-group text-semibold" style="margin:0px!important;">
		Disabled Trainer
		<small class="display-block">All Disabled trainer</small>
		</h3>
	</div>
</div>
<div class="row">
	<?php 
		if(!empty($allDisableTrainer)){

			foreach ($allDisableTrainer as $key => $singleTrainer) {
	?>
	<div class="col-lg-3 col-sm-6">
		<div class="thumbnail">
			<div class="thumb">
				<img src="
				<?php
					if(isset($singleTrainer['image']) && !empty($singleTrainer['image'])){
						echo '../assets/images/trainer/'.$singleTrainer['image'];
					}else{
						echo '../assets/images/trainer/nobody.jpg';
					}

				 ?>" alt="">
				<div class="caption-overflow">
					<span>
						<a href="single_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded ">Details <i class="icon-plus3"></i></a>
					</span>
				</div>
			</div>
			<div class="caption">
				<div class="text-center">
					<h6 class="text-semibold no-margin"><?php echo $singleTrainer['full_name'];?></h6>
						<ul class="list list-unstyled text-right">
							<li class = "text-center">
								<h5><span class="label bg-teal-600">
									<?php
									if($singleTrainer['trainer_status']=='lead_trainer'){
										echo "Lead Trainer in BITM";
									}elseif($singleTrainer['trainer_status'] == 'assist_trainer'){
										echo "Assistant Trainer in BITM";
									}else{
										echo "Lab Assistant in BITM";
										}
									?>
								</span></h5>
							</li>
							<li class = "text-center"><span class="label bg-grey-400"><?php echo $singleTrainer['team']; ?> TEAM</span></li>
						</ul>
					<?php 
						$eduction = unserialize($singleTrainer['edu_status']);
					?>
					<p><?php echo $eduction[0].", ".$eduction[1]."."?></p>
				</div>
				<ul class="list-unstyled list-icons">
					<li>Assigned for <?php echo $singleTrainer['title'];?></li>
					<li><i class="icon-phone position-left"></i> <?php echo $singleTrainer['phone'];?>
					</li>
					<li><i class="icon-mail5 position-left"></i><a href="mailto:<?php echo $singleTrainer['email'];?>"><?php echo $singleTrainer['email'];?></a>
					</li>
				</ul>
				<div class="btn-group">
					<a type="button" class="btn btn-default" href="restore_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>"><i class="icon-reset position-left"></i>Restore
					</a>
					<?php 
				    	if($_SESSION['logged']['is_admin'] == 1){
				    ?>
					<a type="button" class="btn btn-default" href="parment_delete.php?id=<?php echo $singleTrainer['unique_id'];?>" onclick = "return confirm('Are you sure to Parmently delete this trainer?')"><i class="icon-close2 position-left"></i> Delete
					</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
		} // foreach
	} // !empty
	?>
	</div> <!-- row -->
	
	<?php  include_once 'footer.php'; ?>