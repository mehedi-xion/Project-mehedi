<?php
require_once '../../vendor/autoload.php';

use ProjectMehedi\trainers\trainers;

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

if($_SERVER['REQUEST_METHOD'] == 'POST'){


	$objAddTrainers = new trainers();

	$data = $objAddTrainers -> prepare($_POST);
	$objAddTrainers -> required_validation();
	$objAddTrainers -> add_image();

	$objAddTrainers -> addTrainer();

	// echo "<pre>";
	// print_r($_FILES);
	// print_r($data);

	// $objAddTrainers -> session_message('addSuccess');
	// echo "<br>";$objAddTrainers -> session_message('fullName_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('team_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('course_id_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('trainer_status_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('EduTitle_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('organization_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('passingYear_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('phone_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('email_formateInvalid');
	// echo "<br>";
	// $objAddTrainers -> session_message('email_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('website_invalid');
	// echo "<br>";
	// $objAddTrainers -> session_message('website_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('houseAddress_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('district_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('zipCode_required');
	// echo "<br>";
	// $objAddTrainers -> session_message('course_id');
	// echo "<br>";
	// $objAddTrainers -> session_message('team');
	// echo "<br>";
}else{

	header('location:error.php');
	$_SESSION['errorMsg'] = "Sorry wrong url";
}

?>