<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\courses\courses;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objDisableCourse = new courses();
$allCourses = $objDisableCourse->disable_course_list();
$i = 1;
$serial = 1;
include_once '../header.php';
include_once 'menubar.php';
?>


<?php $objDisableCourse -> session_message('deleteCourseSuccess'); ?>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h3 class="panel-title text-center">All Disable Courses</h4>
	</div>
	<div class="panel-body">
		<table class="table datatable-basic datatable-responsive">
			<thead>
				<tr class="disabled bg-slate-600">
					<th>#</th>
					<th class="text-center col-md-3">Course Name</th>
					<th>Duration</th>
					<th>Type</th>
					<th>Course<br>Fee (tk)</th>
					<th colspan="3">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if(isset($allCourses) && !empty($allCourses)){
					foreach ($allCourses as $singleCourses) {
				
				?>
				<tr class="<?php
							echo ($i % 2 == 0)?'alpha-slate':'alpha-grey';
							$i++;
					?>">
					<td><?php echo $serial++; ?></td>
					<td><?php echo $singleCourses['title']; ?></td>
					<td><?php
							if ($singleCourses['duration'] == '15_days') {
							echo "15 Days Long";
							}
							if ($singleCourses['duration'] == '1_month') {
							echo "1 Month Long";
							}
							if ($singleCourses['duration'] == '2_months') {
							echo "2 Months Long";
							}
							if ($singleCourses['duration'] == '3_months') {
							echo "Maximum 3 Months";
							}
					?></td>
					<?php
					if ($singleCourses['course_type'] == 'free') {
					echo '<td><b><span class="label label-flat border-danger text-danger-600"><b>FREE Course</b></span></b></td>';
					} else {
					echo '<td>Paid Course</td>';
					}
					?>
					<td>
						<?php
						if ($singleCourses['course_fee'] == '0') {
						echo'Not Applicable';
						} else {
						echo $singleCourses['course_fee'];
						}
						?>
						<?php
						if ($singleCourses['is_offer'] == 1) {
						echo '<br><span class="badge bg-indigo">Offered</span>';
						}
						?>
					</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="single_course.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-enlarge"></i>
							</a>
							<a href="restore_course.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-reset"></i>
							</a>
							<?php 
						    	if($_SESSION['logged']['is_admin'] == 1){
						    ?>
							<a href="delete_course_parmanent.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"  onclick = "return confirm('Are you sure to parmanent delete this Course?')"><i class="icon-trash"></i>
							</a>
							<?php
								}
							?>
						</div>
					</td>
				</tr>
				<?php
					}// foreach
				}// if $allLabInfo not empty
				?>
			</tbody>
		</table>
	</div>
</div>
<!-- /basic datatable -->
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>
<?php include_once 'footer.php' ?>