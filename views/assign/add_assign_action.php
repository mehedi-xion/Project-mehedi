<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAssignNew = new Assign();


// echo "<pre>";

$objAssignNew -> prepare($_POST);

$objAssignNew -> validate();

$endtime = microtime(true);


$objAssignNew -> date_validate();

$objAssignNew -> assign_new_session();


// ?>