<?php

namespace ProjectMehedi\user\user_login;

use PDO;

if(!class_exists('DatabaseConnection')){ 

	require(dirname(__FILE__).'../../../../src/DatabaseConnection/DatabaseConnection.php');
	 }

// require_once '../../src/DatabaseConnection/DatabaseConnection.php';
 

class user_login extends \DatabaseConnection{

	public $id;
	public $username;
	public $password;
	public $data;
	public $error;

	public function prepare($data = ""){

		if(!empty($data['id'])){
			$this->id = $data['id'];
		}
		if(!empty($data['username'])){
			$this->username = $data['username'];
		}
		if(!empty($data['password'])){
			$this->password = $data['password'];
		}
		if(!empty($data['is_admin'])){
			$this->is_admin = $data['is_admin'];
		}

		return $this;
		
	}// prepare \\

	
	public function login(){
		$query = "SELECT * FROM `users` WHERE `username` = '$this->username' AND `password` = '$this->password'";
		$stmt = $this->conn -> prepare($query);
		$stmt -> execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if(!empty($this->username) && !empty($this->password)){
			if(!empty($row)){
				if($row['is_delete'] == 1){
					$_SESSION['accountSuspended'] = "Your account is suspended. Please contact with BITM admin";
					header('location:../index.php');
				}else{ // is delete = 1
					$_SESSION['logged'] = $row;
					header('location:../assign/list.php');
				}
			}else{ // empty row
				
				$_SESSION['logginError'] = "Username and Password is not matched";
				header('location:../../index.php');
			}
		}else{
			$_SESSION['emptyField'] = "Both Username and Password is required";
			header('location:../../index.php');
		}
	} // login \\


	public function login_check(){

		if(empty($_SESSION['logged']) && !isset($_SESSION['logged'])){

			header('location:../../index.php');
			$_SESSION['loginFirst'] = "Please login first to view other page";
		}
	}




}// Class \\
